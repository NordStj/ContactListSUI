//
//  Person.swift
//  ContactListSUI
//
//  Created by Дмитрий on 14.11.2023.
//

struct Person: Identifiable {
    
    let id: Int
    let name: String
    let lastName: String
    let phone: String
    let email: String
    var fullName: String {
        "\(name) \(lastName)"
    }
    
    static func getContacts() -> [Person] {
        let contacts = DataManager.shared
        var persons: [Person] = []
        
        let names = contacts.names.shuffled()
        let lastName = contacts.lastNames.shuffled()
        let phone = contacts.phone.shuffled()
        let email = contacts.email.shuffled()
        
        for (index, name) in names.enumerated() {
            let id = index
            let lastName = lastName[index]
            let phone = phone[index]
            let emael = email[index]
            
            let person = Person(id: id, name: name, lastName: lastName, phone: phone, email: emael)
            persons.append(person)
        }
        
        
        return persons
        }
        
    static func getContact() -> Person {
        let contacts = DataManager.shared
        var person: Person
        
        let id = 0
        let name = contacts.names.randomElement() ?? ""
        let lastName = contacts.lastNames.randomElement() ?? ""
        let phone = contacts.phone.randomElement() ?? ""
        let emael = contacts.email.randomElement() ?? ""
            
        person = Person(id: id, name: name, lastName: lastName, phone: phone, email: emael)
        
        return person
        }
    }
