//
//  ContactListSUIApp.swift
//  ContactListSUI
//
//  Created by Дмитрий on 14.11.2023.
//

import SwiftUI

@main
struct ContactListSUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
