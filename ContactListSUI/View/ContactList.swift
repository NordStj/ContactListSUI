//
//  ContactList.swift
//  ContactListSUI
//
//  Created by Дмитрий on 14.11.2023.
//

import SwiftUI

struct ContactList: View {
    
    private let persons: [Person] = Person.getContacts()
    
    var body: some View {
        
 
        List(persons) { person in
            NavigationLink(destination: ContactDetails(person: person)){
                Text(person.fullName)
            }
        }
        .listStyle(.plain)
    }
}

#Preview {
    ContactList()
}
