//
//  ContentView.swift
//  ContactListSUI
//
//  Created by Дмитрий on 14.11.2023.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationStack{
            ZStack {
                TabView {
                    ContactList()
                        .tabItem {
                            Image(systemName: "person.2.fill")
                            Text("Contacts")
                        }
                    SecondContactList()
                        .tabItem {
                            Image(systemName: "phone.fill")
                            Text("Numbers")
                        }
                }
            }
            .navigationTitle("Contact List")
        }
    }
}

#Preview {
    ContentView()
}
