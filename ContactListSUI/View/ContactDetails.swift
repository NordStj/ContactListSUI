//
//  ContactDetails.swift
//  ContactListSUI
//
//  Created by Дмитрий on 14.11.2023.
//

import SwiftUI

struct ContactDetails: View {
    
    let person: Person
    
    var body: some View {
        NavigationStack{
            ZStack{
                List{
                    
                    HStack{
                        
                        Spacer()
                        Image(systemName: "person.fill")
                            .resizable()
                            .frame(width: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: .leading)
                        Spacer()
                    }
                    
                    HStack {
                        
                        Image(systemName: "phone")
                            .foregroundColor(.blue)
                        Text("\(person.phone)")
                    }
                    
                    HStack {
                        
                        Image(systemName: "envelope")
                            .foregroundColor(.blue)
                        Text("\(person.email)")
                    }
                        
                }
            }
            .navigationTitle(person.fullName)
        }
    }
}

#Preview {
    ContactDetails(person: Person.getContact())
}
