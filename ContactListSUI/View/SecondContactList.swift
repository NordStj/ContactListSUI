//
//  SecondContactList.swift
//  ContactListSUI
//
//  Created by Дмитрий on 14.11.2023.
//

import SwiftUI

struct SecondContactList: View {
    
    private let persons: [Person] = Person.getContacts()
    
    var body: some View {
        
        List {
            ForEach(persons,id: \.id) { person in
                Section(header: Text(person.fullName)) {
                    
                    HStack{
                        Image(systemName: "phone")
                            .foregroundColor(.blue)
                        Text("\(person.phone)")
                    }
                    
                    HStack{
                        Image(systemName: "envelope")
                            .foregroundColor(.blue)
                        Text("\(person.email)")
                    }
                    
                }
            }
        }
    }
}

#Preview {
    SecondContactList()
}
