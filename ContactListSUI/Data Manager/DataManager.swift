//
//  DataManager.swift
//  ContactListSUI
//
//  Created by Дмитрий on 14.11.2023.
//

class DataManager {
    static let shared = DataManager()
    
    let names = [
        "Nick",
        "Tim",
        "Steven",
        "Jack",
        "John",
        "Michael",
        "Andrew",
        "David",
        "Daniel",
        "Brian"
    ]

    let lastNames = [
        "Cook",
        "Jobs",
        "Rex",
        "Geyts",
        "Smith",
        "Johnson",
        "Anderson",
        "Brown",
        "Davis",
        "Wilson"
    ]

    let phone = [
        "32435433",
        "43535355",
        "76856746",
        "55767685",
        "98765432",
        "12345678",
        "87654321",
        "13579246",
        "24681357",
        "95135742"
    ]

    let email = [
        "afg@mail.eu",
        "oghk@mail.ru",
        "get@gmail.com",
        "timcook@apple.com",
        "johnsmith@yahoo.com",
        "michaelj@gmail.com",
        "andrewa@hotmail.com",
        "davidb@gmail.com",
        "danieldavis@gmail.com",
        "brianwilson@yahoo.com"
    ]
    
    private init() {}
}
